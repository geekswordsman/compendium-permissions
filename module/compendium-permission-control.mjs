import { HelpFormApplication } from "./about/help-form-application.mjs";
import { CONFIG } from "./config.mjs";

export class CompendiumPermissionControl extends HelpFormApplication {
    #compendiumKey = null;
    #compendiumName = null;

    /**
     * constructor
     * @param {*} object - Expects { compendiumKey, compendiumName }
     * @param {*} options - Options to be passed to the FormApplication
     */
    constructor(object, options) {
        if (!object) { object = {} };
        object.enableHelpButton = false;
        object.wikiLink = "https://gitlab.com/geekswordsman/compendium-permissions/-/wikis/home";
        super(object, options);

        this.#compendiumKey = object.compendiumKey;
        this.#compendiumName = object.compendiumName;
    }

    /**
     * defaultOptions - Override for FormApplication to provide defaults for this dialog
     */
    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            closeOnSubmit: false,
            height: 'auto',
            width: '400',
            submitOnChange: true,
            template: "modules/compendium-permissions/templates/compendium-permissions.hbs",
            title: `${game.i18n.localize("PERMISSION.Configure")}: `
        }

        const mergedOptions = foundry.utils.mergeObject(defaults, overrides);

        return mergedOptions;
    }

    /**
     * title - Override to dynamically provide the dialog title based on the compendium's name
     */
    get title() {
        return `${game.i18n.localize("PERMISSION.Configure")}: ${this.#compendiumName}`;
    }

    /**
     * activateListneners - override to listen for our button click
     * @param {*} html - our dialog's HTML
     */
    activateListeners(html) {
        super.activateListeners(html);

        html.on('click', '[data-action]', this._handleButtonClick.bind(this));
    }

    /**
     * getData - override for providing handlebars data
     * @param {*} options - existing options provided by the FormApplication
     * @returns - object of data to provide to the Handlebars template
     */
    getData(options) {
        let formData = game.settings.get(CONFIG.ID, CONFIG.SETTINGS.PermissionData)[this.#compendiumKey];

        let players = [];
        if (!formData) {
            players = game.users.filter(u => !u.isGM).map((u) => { return { id: u.id, name: u.name, accessLevel: 1 }})
        } else {
            players = game.users.filter(u => !u.isGM).map((u) => { return { id: u.id, name: u.name, accessLevel: formData[u.id] ?? 1 }})
        }
        const data = {
            options: CONFIG.ACCESS_LEVELS,
            players
        };

        return data;
    }

    /**
     * _handleButtonClick - handles what to do when one of our buttons are clicked, event set from @activateListeners
     * @param {*} event - The mouse event that triggered this event.
     */
    async _handleButtonClick(event) {
        const clickedElement = $(event.currentTarget);
        const action = clickedElement.data().action;

        switch (action) {
            case "close":
                this.close();
                break;
            default:
                break;
        }
    }

    /**
     * _updateObject - required override from FormApplication
     * @param {*} event - event that triggered this update
     * @param {*} formData - All form information
     */
    async _updateObject(event, formData) {
    }

    /**
     * _onSubmit - override from FormApplication, fires whenever a change on our Form happens
     * @param {*} event - the event that triggers the submit
     * @param {*} updateData - any data provided by the triggering event
     * @param {*} preventClose - does this event prevent a close event
     * @param {*} preventRender - does this event prevent a re-render
     * @returns boolean, true to trickle the event, false to report event has been adequetly handled
     */
    async _onSubmit(event, updateData, preventClose, preventRender) {
        if (!updateData) {
            updateData = this._getSubmitData();
        }

        let permissionData = game.settings.get(CONFIG.ID, CONFIG.SETTINGS.PermissionData);
        if (permissionData.hasOwnProperty(this.#compendiumKey)) {
            delete(permissionData[this.#compendiumKey]);
        }

        let newPermissions = {};
        for(let key of Object.keys(updateData)) {
            newPermissions[key.replace("access-", "")] = (updateData["default"] != -1) ? updateData["default"] : updateData[key];
        }

        permissionData[this.#compendiumKey] = newPermissions;
        await game.settings.set(CONFIG.ID, CONFIG.SETTINGS.PermissionData, permissionData);

        game.socket.emit(`module.${CONFIG.ID}`, {eventName: 'permissionsUpdated'});

        return false;
    }
}