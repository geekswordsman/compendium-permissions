import { Logger } from "./logger/logger.mjs";
import { CompendiumPermissionControl } from "./compendium-permission-control.mjs";
import { CONFIG } from "./config.mjs";
import { AboutDialog } from "./about/about-dialog.mjs";

export class CompendiumPermissions {
    /**
     * initialize - Register game settings
     */
    static initialize() {
        Logger.info(true, "Initializing Compendium Browser!");

        CONFIG.FOUNDRYVERSION = game.version ?? game.data.version;

        game.settings.register(CONFIG.ID, CONFIG.SETTINGS.PermissionData, {
            config: false,
            default: {},
            scope: 'world',
            type: Object
        });

        game.settings.registerMenu(CONFIG.ID, CONFIG.SETTINGS.InfoButton, {
            name: game.i18n.localize("COMPENDIUM-PERMISSIONS.ABOUT.About"),
            label: game.i18n.localize("COMPENDIUM-PERMISSIONS.ABOUT.About"),
            restricted: false,
            icon: 'fas fa-info-circle',
            type: AboutDialog
        })
    }

    /**
     * onReady - Listen for our socket event if we're not a GM
     */
    static onReady() {
        if (!game.user.isGM) {
            Logger.info(false, "Registering for hook call!");
            game.socket.on(`module.${CONFIG.ID}`, CompendiumPermissions.permissionsUpdated);
        }
    }

    /**
     * renderSidebarMenu - If we're a GM, add our context menu when right clicking a compendium
     * @param {*} html - HTML of the sidebar
     * @param {*} options - Array of sidebar menu context items
     * @returns 
     */
    static async renderSidebarMenu(html, options) {
        if (!game.user.isGM) { return; }
        options.push(
            {
                name: "PERMISSION.Configure",
                icon: "<i class='fas fa-lock fa-fw'></i>",
                callback: (selection) => {
                    Logger.debug(false, selection);
                    const pack = selection.data().pack;
                    CompendiumPermissions._configurePermissionDialog(pack);
                }
            }
        );
    }
    
    /**
     * renderCompendiumSidebar - Remove compendiums from the sidebar, but only if we're actually on the compendium sidebar and not a GM.
     * @param {*} app - Foundry class for the sidebar
     * @param {*} html - the HTML of the sidebar
     * @returns 
     */
    static async renderCompendiumSidebar(app, html) {
        if (app.options.id !== "compendium" || game.user.isGM) { return; }
        let options = html.find(".compendium-pack");
        let permissionData = game.settings.get(CONFIG.ID, CONFIG.SETTINGS.PermissionData);
        let toRemove = [];
        for (let i = 0; i < options.length; i++) {
            if (!CompendiumPermissions._hasPermission(options[i], permissionData)) { toRemove.push(options[i]); }
        }

        for (let ele of toRemove) { ele.parentNode.removeChild(ele); }
    }

    /**
     * permissionsUpdated - Emitter event to force update the compendium sidebar when permissions are updated 
     * @returns
     */
    static permissionsUpdated() {
        Logger.debug(false, "Permissions Update Called!");
        if (game.user.isGM) { return; }

        ui.compendium.render(true);
    }

    /**
     * _hasPermission - Check if the logged in user has permission to view the provided compendium pack element 
     * @param {*} html - The compendium pack's <li> element
     * @param {*} permissionData - Our permission data to examine
     * @returns - True if the user has permission, otherwise false.
     */
    static _hasPermission(html, permissionData) {
        let userID = game.user.id;
        let key = html.dataset.pack;

        if (!permissionData[key] || !permissionData[key].hasOwnProperty(userID)) { return true; }

        return permissionData[key][userID] > 0;
    }

    /**
     * _configurePermissionDialog
     * @param {*} compendiumKey - Foundry key for the compendium we are configuring permissions for.
     */
    static _configurePermissionDialog(compendiumKey) {
        const pack = game.packs.get(compendiumKey);
        const dialog = new CompendiumPermissionControl({compendiumKey, compendiumName: pack.metadata.label});
        dialog.render(true);
    }
}