export const CONFIG = {};

CONFIG.ID = "compendium-permissions";

CONFIG.FOUNDRYVERSION = 0;

CONFIG.SETTINGS = {
    InfoButton: "Info Button",
    PermissionData: "PermissionData"
}

CONFIG.ACCESS_LEVELS = {
    0: "COMPENDIUM-PERMISSIONS.DIALOG.Hidden" ,
    1: "COMPENDIUM-PERMISSIONS.DIALOG.CanAccess"
}