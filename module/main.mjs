import { CONFIG } from "./config.mjs";
import { Logger } from "./logger/logger.mjs";
import { CompendiumPermissions } from "./compendium-permissions.mjs";

Logger.MODULE_ID = CONFIG.ID;

/*Enable Debug Module */
Hooks.once('devModeReady', ({ registerPackageDebugFlag }) => {
    registerPackageDebugFlag(CONFIG.ID);
});

/* Initialize */
Hooks.once('init', () => { CompendiumPermissions.initialize(); });

Hooks.once('ready', () => { CompendiumPermissions.onReady(); })

/* Compendium Sidebar Context Menu */
Hooks.on('getCompendiumDirectoryEntryContext', async (html, options) => { CompendiumPermissions.renderSidebarMenu(html, options) });

/* Remove Compendium Entries */
Hooks.on('renderSidebarTab', async (app, html) => { CompendiumPermissions.renderCompendiumSidebar(app, html) });