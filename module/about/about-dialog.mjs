import { CONFIG } from "../config.mjs";

export class AboutDialog extends FormApplication {
    constructor(object, options) {
        super(object, options);
    }

    static get defaultOptions() {
        const defaults = super.defaultOptions;

        const overrides = {
            id: "CompendiumPermissionsAboutDialog",
            closeOnSubmit: false,
            height: "auto",
            width: 550,
            submitOnChange: true,
            template: `modules/${CONFIG.ID}/module/about/about-dialog.hbs`,
            title: game.i18n.localize("COMPENDIUM-PERMISSIONS.ABOUT.About"),
        };

        return foundry.utils.mergeObject(defaults, overrides);
    }

    activateListeners(html) {
        super.activateListeners(html);
    }

    getData(options) {
        return {
            "version": game.modules.get(CONFIG.ID).data.version
        };
    }
}