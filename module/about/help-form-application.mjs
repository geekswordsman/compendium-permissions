/** 
 * Help Form 
 * Extends Foundry's FormApplication to provide a popup help context for the window.
 **/

import { CONFIG } from "../config.mjs";
import { AboutDialog } from "./about-dialog.mjs";
import { Logger } from "../logger/logger.mjs";

export class HelpFormApplication extends FormApplication {
    #enableAboutButton = true;
    #enableHelpButton = true;
    #wikiLink = null;
    #tutorialClass = null;

    constructor(object, options) { super(object, options); 
        this.#enableAboutButton = object?.enableAboutButton ?? true;
        this.#enableHelpButton = object?.enableHelpButton ?? true;
        this.#wikiLink = object?.wikiLink ?? null;
        this.#tutorialClass = object?.tutorialClass ?? null;
    }

    _getHeaderButtons() {
        let buttons = super._getHeaderButtons();

        if (this.#enableHelpButton && (this.#wikiLink || (isNewerVersion(CONFIG.FOUNDRYVERSION, "10") && this.#tutorialClass))) {
            buttons.unshift({
                label: "",
                class: "helpFormApplicationHelpButton",
                title: game.i18n.localize("COMPENDIUM-PERMISSIONS.ABOUT.Help"),
                icon: "fas fa-question",
                onclick: () => { this._onHelpRequest(); }
            });
        }

        if (this.#enableAboutButton) {
            buttons.unshift({
                label: "",
                class: "helpFormApplicationAboutButton",
                title: game.i18n.localize("COMPENDIUM-PERMISSIONS.ABOUT.About"),
                icon: "fas fa-info-circle",
                onclick: () => { this._onAboutRequest(); }
            })
        }

        return buttons;
    }

    _onAboutRequest() {
        let dialog = new AboutDialog();
        dialog.render(true);
    }

    _onHelpRequest() {
        if (isNewerVersion(CONFIG.FOUNDRYVERSION, "10")) {
            if (!this.#tutorialClass) {
                window.open(this.#wikiLink, '_blank');
            } else {
                Logger.debug(false, "Load the tutorial!");
            }
        } else {
            window.open(this.#wikiLink, '_blank');
        }
    }
}