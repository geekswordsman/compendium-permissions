# Compendium Permissions

Adds per player permission control to the Compendium Sidebar.

Simply right click on any compendium and select "Configure Permissions" and set either per player permission (Hidden or Can Access), or mass change all player permissions.

Compendium Visibility will override these settings.  If a compendium is not visible to a player, but you give them permission, then the compendium will remain hidden from them.